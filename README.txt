Migrate pkg-java repositories to Git
------------------------------------

This script automates the migration of a Java package managed in the Subversion
repository on alioth to a Git repository.

The script performs the following tasks:

1. Import the SVN repository with its tags into a local Git repository and
   create an empty upstream branch ready to be used with git-import-orig
2. Update the Vcs-* fields in debian/control
3. Add an entry in debian/changelog mentioning the move
4. Push the new Git repository
5. Add a MOVED_TO_GIT.txt file in the SVN repository documenting the migration


Usage
-----

Run:

    ./migrate-svn-repo-to-git <packagename>


Prerequisites
-------------

git-svn must be installed before running this script:

    apt-get install git-svn

The script assumes that SSH is properly configured on the local machine such
that connections to alioth can be performed without specifying the username.
