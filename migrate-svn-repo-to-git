#!/bin/bash
#
# Script to migrate a package from SVN to Git on Alioth.
# This script is tailored for the packages maintained by the Java team.
#

if [ -z "$1" ]; then
    echo "Usage: ./migrate-svn-repo-to-git <packagename>"
    exit 1;
fi

PACKAGE=$1
TEAM=pkg-java

# Create an empty Git repository with an upstream branch
echo "Initializing the Git repository..."
git init $PACKAGE
pushd $PACKAGE
git checkout --orphan upstream
git commit --allow-empty -m 'Initial upstream branch.'
git checkout --orphan master

git remote add origin git+ssh://git.debian.org/git/$TEAM/$PACKAGE

# Clone the SVN repository
echo "Cloning the SVN repository.."
git svn clone --authors-file=../$TEAM-authors.txt \
              --no-metadata \
              --trunk=trunk/$PACKAGE \
              --tags=tags/$PACKAGE \
              svn+ssh://svn.debian.org/svn/$TEAM/ \
              .
git reset --hard

# Substitutions from
# https://sources.debian.net/src/git-buildpackage/0.7.5/gbp/deb/git.py/#L164
sanitize_version() {
    echo "$1" | sed -e 's|%3A|%|g' -e 's|%7E|_|g'
}

# Turn the branches into tags
echo "Converting branches to tags..."
for branch in `git branch -r`; do
    if [ `echo $branch | egrep "tags/.+$"` ]; then
        version=`basename $branch`
        subject=`git log -1 --pretty=format:"%s" $branch`
        GIT_COMMITTER_DATE=`git log -1 --pretty=format:"%ci" $branch` \
            git tag -f -m "$subject" "debian/$(sanitize_version "$version")" "$branch^"

        git branch -d -r $branch
    fi
done

# Remove the remotes/origin/trunk reference
git branch -rd origin/trunk

echo ""
echo "Tags:"
git tag -n

echo ""
echo "Branches:"
git branch -va

# Update the Vcs-* fields
echo ""
echo "Updating debian/control..."
sed -i "s#https\?://anonscm.debian.org/viewvc/$TEAM/trunk/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1.git#g" debian/control
sed -i "s#https\?://svn.debian.org/viewsvn/$TEAM/trunk/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1.git#g" debian/control
sed -i "s#https\?://svn.debian.org/wsvn/$TEAM/trunk/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1.git#g" debian/control

sed -i "s#Vcs-Svn: svn://anonscm.debian.org/$TEAM/trunk/\([^/]*\)/\?#Vcs-Git: https://anonscm.debian.org/git/$TEAM/\1.git#g" debian/control
sed -i "s#Vcs-Svn: svn://svn.debian.org/svn/$TEAM/trunk/\([^/]*\)/\?#Vcs-Git: https://anonscm.debian.org/git/$TEAM/\1.git#g" debian/control
sed -i "s#Vcs-Svn: svn://svn.debian.org/$TEAM/trunk/\([^/]*\)/\?#Vcs-Git: https://anonscm.debian.org/git/$TEAM/\1.git#g" debian/control

echo "Updated Vcs-* fields:"
grep Vcs- debian/control

# Document the migration in debian/changelog
dch 'Moved the package to Git'

# Commit the changes
git commit debian/control debian/changelog -m 'Moved the package to Git'

echo ""
while true; do
    read -p "Do you want to push the migrated repository now? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * ) exit;;
    esac
done

# Create the Git repository on alioth
echo "Creating the Git repository on alioth..."
ssh alioth.debian.org "cd /srv/git.debian.org/git/$TEAM && ./setup-repository $PACKAGE && mv $PACKAGE.git/hooks/post-receive $PACKAGE.git/hooks/post-receive.disabled"

# Push the new repository
echo "Pushing the new repository..."
git push --all
git push --tags

# Track the remote branches
git branch --set-upstream-to=origin/master master
git branch --set-upstream-to=origin/upstream upstream

# Re-enable the post receive hook
ssh alioth.debian.org "cd /srv/git.debian.org/git/$TEAM && mv $PACKAGE.git/hooks/post-receive.disabled $PACKAGE.git/hooks/post-receive"

# Add a file in the SVN repository documenting the migration
echo "Marking the SVN repository as obsolete..."
svn checkout svn+ssh://svn.debian.org/svn/$TEAM/trunk/$PACKAGE . --depth empty
echo "The $PACKAGE package is now maintained in a Git repository.
Please clone git+ssh://anonscm.debian.org/git/$TEAM/$PACKAGE to work on this package.
" > MOVED_TO_GIT.txt

svn add MOVED_TO_GIT.txt
svn commit MOVED_TO_GIT.txt -m "Moved $PACKAGE to Git"
rm -Rf MOVED_TO_GIT.txt .svn

echo ""
echo "Migration completed"
echo "New repository located at git+ssh://git.debian.org/git/$TEAM/$PACKAGE"
echo ""
