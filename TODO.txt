TODO

- Connect to alioth and check if the repository has already been converted before starting the migration
- Add a command line parameter to push the converted repository without prompting the user
- Turn the script into a proper package
- Import the lastest upstream tarball into the upstream branch and tag the release
